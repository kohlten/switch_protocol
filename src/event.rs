pub enum ServerEvent<T> {
    /// Client Received a packet.
    ReceivedPacket(ServerPacket<T>),
    ClientConnected(ClientConnected),
    ClientDisconnected(ClientDisconnected)
}

pub struct ServerPacket<T> {
    /// Id of the client.
    id: String,
    packet: T,
}

pub struct ClientConnected {
    id: String,
    local_addr: String,
    remote_addr: String,
}

pub struct ClientDisconnected {
    id: String
}

impl<T> ServerPacket<T> {
    #[cfg(feature = "internal")]
    pub fn new(id: &str, packet: T) -> Self {
        ServerPacket { id: id.to_string(), packet }
    }

    #[cfg(not(feature = "internal"))]
    pub(crate) fn new(id: &str, packet: T) -> Self {
        ServerPacket { id: id.to_string(), packet }
    }

    pub fn get_id(&self) -> &str {
        &self.id
    }

    pub fn get_packet(&self) -> &T {
        &self.packet
    }
}

impl ClientDisconnected {
    #[cfg(not(feature = "internal"))]
    pub(crate) fn new(id: &str) -> Self {
        Self {
            id: id.to_string(),
        }
    }

    #[cfg(feature = "internal")]
    pub fn new(id: &str) -> Self {
        Self {
            id: id.to_string(),
        }
    }

    pub fn get_id(&self) -> &str {
        &self.id
    }
}

impl ClientConnected {
    #[cfg(not(feature = "internal"))]
    pub fn new(id: &str, remote_addr: &str, local_addr: &str) -> Self {
        Self {
            id: id.to_string(),
            local_addr: local_addr.to_string(),
            remote_addr: remote_addr.to_string(),
        }
    }

    #[cfg(feature = "internal")]
    pub fn new(id: &str, remote_addr: &str, local_addr: &str) -> Self {
        Self {
            id: id.to_string(),
            local_addr: local_addr.to_string(),
            remote_addr: remote_addr.to_string(),
        }
    }

    pub fn get_id(&self) -> &str {
        &self.id
    }

    pub fn get_local_addr(&self) -> &str {
        &self.local_addr
    }

    pub fn get_remote_addr(&self) -> &str {
        &self.remote_addr
    }
}