use std::ops::{Deref, DerefMut};

use crate::error::SwitchError;
use crate::internal::socket::Socket;

/// Conn wraps a stream and reads and writes to the stream on update.
pub struct Conn<S>
    where S: Socket {
    conn: S,
    write: Vec<u8>,
}

impl<S: Socket> Conn<S> {
    pub fn new(conn: S) -> Self {
        conn.set_nonblocking(true).unwrap();
        Self {
            conn,
            write: Vec::new(),
        }
    }

    /// Write data to the internal buffer. The data won't be sent until a call to [Self::update]
    pub fn write(&mut self, data: &[u8]) -> Result<(), SwitchError> {
        if self.write.len() > 0 {
            self.write.extend_from_slice(data);
            return Ok(());
        }
        let size = match self.conn.write(data) {
            Ok(size) => {
                Ok(size)
            }
            Err(e) => { Err(e) }
        }?;
        if data.len() - size > 0 {
            self.write.extend_from_slice(&data[size..]);
        }
        Ok(())
    }

    /// Read data from the internal buffer.
    pub fn read(&mut self, buf: &mut [u8]) -> Result<usize, SwitchError> {
        match self.conn.read(buf) {
            Ok(size) => { Ok(size) }
            Err(e) => { Err(e) }
        }
    }

    /// Read and write from the stream to and from the internal buffers.
    pub fn update(&mut self) -> Result<(), SwitchError> {
        if self.write.len() > 0 {
            match self.conn.write(&self.write) {
                Ok(size) => {
                    self.write.drain(..size);
                }
                Err(e) => { return Err(e); }
            }
        }
        Ok(())
    }

    pub fn peer_addr(&self) -> Result<String, SwitchError> {
        self.conn.peer_addr()
    }

    pub fn local_addr(&self) -> Result<String, SwitchError> {
        self.conn.local_addr()
    }
}

impl<S: Socket> Deref for Conn<S> {
    type Target = S;

    fn deref(&self) -> &Self::Target {
        &self.conn
    }
}

impl<S: Socket> DerefMut for Conn<S> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.conn
    }
}