use crate::error::SwitchError;

/// Socket allows you to add a stream to the protocol. See [crate::tcp::TcpSocket] for details on how to implement.
pub trait Socket {
    /// Reads data from the socket.
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, SwitchError>;

    /// Writes data to the socket.
    fn write(&mut self, buf: &[u8]) -> Result<usize, SwitchError>;

    /// Makes this socket nonblocking. This is important because the library is not multithreaded, so each call to read and write cannot block.
    fn set_nonblocking(&self, nonblocking: bool) -> Result<(), SwitchError>;

    /// Local address of this socket.
    fn local_addr(&self) -> Result<String, SwitchError>;

    /// Peer address of this socket.
    fn peer_addr(&self) -> Result<String, SwitchError>;
}