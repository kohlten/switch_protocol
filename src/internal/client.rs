use crate::error::SwitchError;

/// This trait allows you to define a client to use. See [crate::tcp::TcpClient] for details on how you can implement this.
pub trait Client<T> { /// Updates the state of the client
    fn update(&mut self) -> Result<Option<T>, SwitchError>;

    /// Sends a packet to the connected person
    fn send(&mut self, packet: &[u8]) -> Result<(), SwitchError>;

    /// Returns the peer address of the connected person.
    fn peer_addr(&self) -> Result<String, SwitchError>;

    /// Returns the local address of this stream
    fn local_addr(&self) -> Result<String, SwitchError>;
}