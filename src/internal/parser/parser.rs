//! Parser controls the state of the data flowing in. A packet is just simply a integer with a number of bytes, and the data attached to it.
//! The parser will go through the input data, waiting for the specified bytes in the header.
//! There is no data checking done here, so that is outside of the scope of this library. For that, you can use a protocol that has reliability (tcp).

use std::io::Cursor;
use byteorder::{LittleEndian, ReadBytesExt};

pub const HEADER_SIZE: usize = 4;


#[derive(PartialEq)]
enum State {
    WaitingForHeader,
    WaitingForData,
}

pub struct Parser {
    state: State,
    data: Vec<u8>,
    current: Option<u32>,
}

impl Parser {
    pub fn new() -> Parser {
        Parser {
            state: State::WaitingForHeader,
            data: Vec::new(),
            current: None,
        }
    }

    pub fn len(&self) -> usize {
        self.data.len()
    }

    /// Adds data to this parser
    pub fn add_data(&mut self, data: &[u8]) {
        self.data.extend_from_slice(data);
    }

    /// Updates the state. If there is data available, will try to parse the header and the body.
    pub fn update(&mut self) -> Option<Vec<u8>> {
        if self.state == State::WaitingForHeader {
            if self.do_header() {
                //println!("waiting for packet of size {}", self.current.as_ref().unwrap().size);
                // Switch to Data state
                self.state = State::WaitingForData
            }
        }
        if self.state == State::WaitingForData {
            if let Some(packet) = self.do_body() {
                // Switch to Header state
                self.state = State::WaitingForHeader;
                return Some(packet);
            }
        }
        None
    }

    /// Tries to parse the header from the data available. Will always eat the bytes it reads.
    fn do_header(&mut self) -> bool {
        if self.data.len() < HEADER_SIZE {
            return false;
        }
        let data = &self.data[..HEADER_SIZE];
        let mut stream = Cursor::new(data);
        let size = stream.read_u32::<LittleEndian>().unwrap();
        self.data.drain(..HEADER_SIZE);
        self.current = Some(size);
        true
    }

    /// Tries to parse the body and eats the data if it can.
    fn do_body(&mut self) -> Option<Vec<u8>> {
        let size = self.current.unwrap();
        if (self.data.len() as u32) < size {
            return None;
        }
        Some(self.data.drain(..size as usize).collect())
    }
}