use std::ops::{Deref, DerefMut};
use byteorder::{ByteOrder, LittleEndian};

use crate::error::SwitchError;
use crate::get_buf_size;
use crate::internal::{Conn, Socket};
use crate::internal::parser::{HEADER_SIZE, Parser};

/// Reads and writes packets to the internal connection.
pub struct Protocol<S>
    where S: Socket {
    conn: Conn<S>,
    parser: Parser,
    read_buf: Vec<u8>,
}

impl<S: Socket> Protocol<S> {
    pub fn new(conn: S) -> Self {
        Protocol {
            conn: Conn::new(conn),
            parser: Parser::new(),
            read_buf: vec![0; get_buf_size()],
        }
    }

    /// Updates the connection [crate::internal::Conn::update], adds any incoming data to the parser, and parses the data if it can.
    pub fn update(&mut self) -> Result<Option<Vec<u8>>, SwitchError> {
        match self.conn.update() {
            Ok(_) => {}
            Err(e) => {
                return Err(e);
            }
        }
        let size = self.conn.read(&mut self.read_buf)?;
        if size > 0 {
            self.parser.add_data(&self.read_buf[..size]);
        }
        Ok(self.parser.update())
    }

    /// Sends a packet to the stream.
    pub fn send(&mut self, packet: &[u8]) -> Result<(), SwitchError> {
        let mut header_data = [0; HEADER_SIZE];
        LittleEndian::write_u32_into(&[packet.len() as u32], &mut header_data);
        self.conn.write(&header_data)?;
        self.conn.write(packet)
    }

    pub fn peer_addr(&self) -> Result<String, SwitchError> {
        self.conn.peer_addr()
    }

    pub fn local_addr(&self) -> Result<String, SwitchError> {
        self.conn.local_addr()
    }
}

impl<S: Socket> Deref for Protocol<S> {
    type Target = S;

    fn deref(&self) -> &Self::Target {
        &self.conn
    }
}

impl<S: Socket> DerefMut for Protocol<S> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.conn
    }
}