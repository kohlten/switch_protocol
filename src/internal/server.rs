use crate::error::SwitchError;
use crate::ServerEvent;

/// This trait allows you to define a server to use. See [crate::tcp::TcpServer] for details on how you can implement this.
pub trait Server<T> {
    /// Updates the state of the server
    fn update(&mut self) -> Result<(), SwitchError>;

    /// Polls for any events happening
    fn poll(&mut self) -> Option<ServerEvent<T>>;

    /// Closes a stream to a client
    fn close_client(&mut self, id: &str);

    /// Sends a packet to the client at id
    fn send(&mut self, id: &str, packet: &[u8]) -> Result<(), SwitchError>;

    /// Returns the client's peer address
    fn client_peer_addr(&self, id: &str) -> Result<String, SwitchError>;

    /// Returns the client's local address
    fn client_local_addr(&self, id: &str) -> Result<String, SwitchError>;
}