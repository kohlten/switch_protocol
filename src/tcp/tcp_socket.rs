use std::io::{ErrorKind, Read, Write};
use std::net::{TcpStream};

use crate::error::SwitchError;
use crate::internal::Socket;

/// Wrapper around a tcp stream.
pub struct TcpSocket {
    conn: TcpStream,
}

impl TcpSocket {
    pub fn new(conn: TcpStream) -> Self {
        Self { conn }
    }
}

impl Socket for TcpSocket {
    /// Reads data from the socket.
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, SwitchError> {
        match self.conn.read(buf) {
            Ok(size) => {
                Ok(size)
            }
            Err(err) => {
                if err.kind() != ErrorKind::WouldBlock {
                    Err(SwitchError::os_error_from_error(err))
                } else {
                    Ok(0)
                }
            }
        }
    }

    /// Writes data to the socket.
    fn write(&mut self, buf: &[u8]) -> Result<usize, SwitchError> {
        match self.conn.write(buf) {
            Ok(size) => {
                Ok(size)
            }
            Err(err) => {
                if err.kind() != ErrorKind::WouldBlock {
                    Err(SwitchError::os_error_from_error(err))
                } else {
                    Ok(0)
                }
            }
        }
    }

    /// Sets the socket to non-blocking.
    fn set_nonblocking(&self, nonblocking: bool) -> Result<(), SwitchError> {
        match self.conn.set_nonblocking(nonblocking) {
            Ok(_) => { Ok(()) }
            Err(err) => { Err(SwitchError::os_error_from_error(err)) }
        }
    }

    fn local_addr(&self) -> Result<String, SwitchError> {
        match self.conn.local_addr() {
            Ok(addr) => { Ok(addr.to_string()) }
            Err(err) => { Err(SwitchError::os_error_from_error(err)) }
        }
    }

    fn peer_addr(&self) -> Result<String, SwitchError> {
        match self.conn.peer_addr() {
            Ok(addr) => { Ok(addr.to_string()) }
            Err(err) => { Err(SwitchError::os_error_from_error(err)) }
        }
    }
}

