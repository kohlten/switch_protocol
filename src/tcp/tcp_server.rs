use std::collections::{HashMap, VecDeque};
use std::{io, thread};
use std::io::{Error, ErrorKind};
use std::net::{TcpListener, TcpStream};
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread::sleep;
use std::time::{Duration, Instant};

use crate::{ClientConnected, ClientDisconnected, ServerEvent, ServerPacket, SwitchError};
use crate::internal::{Client, Server};
use crate::tcp::TcpClient;

pub type TcpServerEvent = ServerEvent<Vec<u8>>;

/// Wrapper around a TcpListener to be used as a Protocol Server.
pub struct TcpServer {
    client_recv: Receiver<io::Result<TcpStream>>,
    running: Arc<AtomicBool>,
    events: VecDeque<TcpServerEvent>,
    clients: HashMap<String, TcpClient>,
    received: usize,
}

impl TcpServer {
    /// Creates a new TcpServer and sets the listener to non-blocking.
    pub fn new(listener: TcpListener) -> TcpServer {
        let (client_send, client_recv) = channel();
        let running = Arc::new(AtomicBool::new(true));
        let running_clone = running.clone();
        thread::spawn(move || {
           async_accept(listener, client_send, running_clone);
        });
        TcpServer {
            client_recv,
            running,
            events: VecDeque::new(),
            clients: HashMap::new(),
            received: 0,
        }
    }
}

impl Server<Vec<u8>> for TcpServer {
    /// Checks for any incoming connections, updates each client, and checks for packets on each client.
    fn update(&mut self) -> Result<(), SwitchError> {
        match self.client_recv.try_recv() {
            Ok(res) => {
                match res {
                    Ok(sock) => {
                        let id = id_generator::generate_rand_id();
                        let client = TcpClient::new(sock);
                        self.events.push_back(ServerEvent::ClientConnected(ClientConnected::new(&id, client.peer_addr().unwrap().as_str(), client.local_addr().unwrap().as_str())));
                        self.clients.insert(id, client);
                    }
                    Err(err) => {
                        return Err(SwitchError::os_error_from_error(err));
                    }
                }
            }
            Err(_) => {}
        }
        let mut to_be_removed = Vec::new();
        for (id, client) in self.clients.iter_mut() {
            match client.update() {
                Err(_e) => {
                    to_be_removed.push(id.to_string());
                    continue;
                }
                Ok(packet) => {
                    match packet {
                        None => {}
                        Some(packet) => {
                            self.received += 1;
                            self.events.push_back(ServerEvent::ReceivedPacket(ServerPacket::new(id, packet)));
                        }
                    }
                }
            }
        }
        for id in to_be_removed.iter() {
            self.clients.remove(id).unwrap();
            self.events.push_back(ServerEvent::ClientDisconnected(ClientDisconnected::new(id)));
        }
        Ok(())
    }

    /// Pops a event from the queue if there is one.
    fn poll(&mut self) -> Option<TcpServerEvent> {
        self.events.pop_front()
    }

    /// Closes a client at id.
    fn close_client(&mut self, id: &str) {
        self.clients.remove(id);
    }

    /// Sends a packet with client at id.
    fn send(&mut self, id: &str, packet: &[u8]) -> Result<(), SwitchError> {
        if let Some(client) = self.clients.get_mut(id) {
            return client.send(packet);
        }
        Err(SwitchError::NoClientWithId(id.to_string()))
    }

    fn client_peer_addr(&self, id: &str) -> Result<String, SwitchError> {
        if self.clients.contains_key(id) {
            let client = self.clients.get(id).unwrap();
            client.peer_addr()
        } else {
            Err(SwitchError::NoClientWithId(id.to_string()))
        }
    }

    fn client_local_addr(&self, id: &str) -> Result<String, SwitchError> {
        if self.clients.contains_key(id) {
            let client = self.clients.get(id).unwrap();
            client.local_addr()
        } else {
            Err(SwitchError::NoClientWithId(id.to_string()))
        }
    }
}

impl Drop for TcpServer {
    fn drop(&mut self) {
        self.running.store(false, Ordering::Relaxed);
    }
}

fn async_accept(listener: TcpListener, client_chan: Sender<io::Result<TcpStream>>, running: Arc<AtomicBool>) {
    listener.set_nonblocking(true).unwrap();
    while running.load(Ordering::Relaxed) {
        match listener.accept() {
            Ok((stream, _)) => {
                match client_chan.send(Ok(stream)) {
                    Ok(_) => {}
                    Err(_) => {
                        let _ = client_chan.send(Err(Error::new(ErrorKind::BrokenPipe, "")));
                        running.store(false, Ordering::Relaxed);
                    }
                }
            }
            Err(err) => {
                match err.kind() {
                    ErrorKind::WouldBlock => {}
                    _ => {
                        let _ = client_chan.send(Err(Error::new(ErrorKind::BrokenPipe, "")));
                        running.store(false, Ordering::Relaxed);
                    }
                }
            }
        }
        sleep(Duration::from_millis(1));
    }
}