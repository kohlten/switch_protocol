use std::net::{TcpStream};

use crate::internal::{Client, Protocol};
use crate::SwitchError;
use crate::tcp::tcp_socket::TcpSocket;

/// TcpClient allows for a Tcp stream to be used with the protocol.
pub struct TcpClient {
    conn: Protocol<TcpSocket>,
}

impl TcpClient {
    pub fn new(conn: TcpStream) -> Self {
        Self {
            conn: Protocol::new(TcpSocket::new(conn)),
        }
    }
}

impl Client<Vec<u8>> for TcpClient {
    /// Updates the Protocol
    fn update(&mut self) -> Result<Option<Vec<u8>>, SwitchError> {
        self.conn.update()
    }

    /// Sends a packet to the connected person.
    fn send(&mut self, packet: &[u8]) -> Result<(), SwitchError> {
        self.conn.send(packet)
    }

    fn peer_addr(&self) -> Result<String, SwitchError> {
        self.conn.peer_addr()
    }

    fn local_addr(&self) -> Result<String, SwitchError> {
        self.conn.local_addr()
    }
}