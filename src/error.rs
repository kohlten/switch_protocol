use std::error::Error;
use std::fmt::{Display, Formatter};

#[derive(Debug)]
pub enum SwitchError {
    NoClientWithId(String),
    OsError(i32),
    Other(Box<dyn Error + Send>),
}

impl SwitchError {
    #[cfg(feature = "internal")]
    pub fn os_error_from_error(err: std::io::Error) -> SwitchError {
        SwitchError::OsError(err.raw_os_error().unwrap_or(-1))
    }

    #[cfg(not(feature = "internal"))]
    pub(crate) fn os_error_from_error(err: std::io::Error) -> SwitchError {
        SwitchError::OsError(err.raw_os_error().unwrap_or(-1))
    }
}

impl Display for SwitchError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            SwitchError::NoClientWithId(id) => { f.write_str(&format!("Client was not found with id {}", id)) }
            SwitchError::OsError(id) => { f.write_str(&format!("Error from os: {}", id)) }
            SwitchError::Other(err) => { f.write_fmt(format_args!("Other Error: {}", err)) }
        }
    }
}

impl Error for SwitchError {}

