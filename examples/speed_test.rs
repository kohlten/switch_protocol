use std::io::{ErrorKind, Read, Write};
use std::net::{TcpListener, TcpStream};
use std::thread::sleep;
use std::time::{Duration, Instant};

use switch_protocol::{Client, Server, ServerEvent, set_buf_size, tcp::TcpClient, tcp::TcpServer};

fn main() {
    let duration = 10;
    let buf_size = 32000;
    let conn_speed = do_conn_speed(duration, buf_size, buf_size);
    print_stats("conn", conn_speed, duration, buf_size, buf_size);
    sleep(Duration::from_secs(3));
    let conn_speed = do_tcp(duration, buf_size, buf_size);
    print_stats("conn", conn_speed, duration, buf_size, buf_size);
    sleep(Duration::from_secs(3));
    /*let conn_speed = do_conn_speed(duration, buf_size, buf_size);
    print_stats("conn", conn_speed, duration, buf_size, buf_size);
    sleep(Duration::from_secs(3));
    let conn_speed = do_conn_speed(duration, buf_size, buf_size);
    print_stats("conn", conn_speed, duration, buf_size, buf_size);
    sleep(Duration::from_secs(3));*/
    /*let conn_speed = do_tcp(duration, buf_size, buf_size);
    print_stats("tcp", conn_speed, duration, buf_size, buf_size);*/
}

fn print_stats(name: &str, speed: usize, duration: u64, buf_size: u64, packet_size: u64) {
    let gbs = speed as f64 / 1000000000.0;
    println!("Buf Size: {}, Packet Size: {}", buf_size, packet_size);
    println!("{} Total GB {} over {} seconds", name, gbs, duration);
    println!("{} GB per second {}", name, gbs / duration as f64);
}

fn do_conn_speed(duration: u64, buf_size: u64, packet_size: u64) -> usize {
    set_buf_size(buf_size as usize);
    let mut server = TcpServer::new(TcpListener::bind("0.0.0.0:8080").unwrap());
    let mut client = TcpClient::new(TcpStream::connect("127.0.0.1:8080").unwrap());
    let mut bytes = 0;
    let buf = vec![255; packet_size as usize];
    let st = Instant::now();
    let dur = Duration::from_secs(duration);
    while Instant::now().duration_since(st) < dur {
        server.update().unwrap();
        client.update().unwrap();
        client.send(&buf).unwrap();
        while let Some(event) = server.poll() {
            match event {
                ServerEvent::ReceivedPacket(packet) => {
                    bytes += packet.get_packet().len();
                }
                _ => {}
            }
        }
    }
    bytes
}

#[allow(dead_code)]
fn do_tcp(duration: u64, _: u64, packet_size: u64) -> usize {
    let server = TcpListener::bind("0.0.0.0:7080").unwrap();
    let mut client = TcpStream::connect("127.0.0.1:7080").unwrap();
    let (mut server_client, _) = server.accept().unwrap();
    client.set_nonblocking(true).unwrap();
    server_client.set_nonblocking(true).unwrap();
    let buf = vec![255; packet_size as usize];
    let mut read = vec![0; packet_size as usize];
    let st = Instant::now();
    let dur = Duration::from_secs(duration);
    let mut bytes = 0;
    while Instant::now().duration_since(st) < dur {
        match client.write(&buf) {
            Ok(_) => {}
            Err(err) => {
                if err.kind() != ErrorKind::WouldBlock {
                    panic!("{}", err);
                }
            }
        }
        bytes += match server_client.read(&mut read) {
            Ok(size) => { size }
            Err(err) => {
                if err.kind() != ErrorKind::WouldBlock {
                    panic!("{}", err);
                }
                0
            }
        };
    }
    bytes
}
