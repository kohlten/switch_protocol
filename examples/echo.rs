use std::net::{TcpListener, TcpStream};
use std::thread::sleep;
use std::time::Duration;

use switch_protocol::{Client, ConnectionStatus, Server, ServerEvent};
use switch_protocol::tcp::{TcpClient, TcpServer};

fn main() {
    // Create the tcp server
    let mut server = TcpServer::new(TcpListener::bind("127.0.0.1:8080").unwrap());
    // Connect to the tcp server
    let mut client = TcpClient::new(TcpStream::connect("127.0.0.1:8080").unwrap());
    // Send the first packet to the server to start the echo.
    client.send("Hello world!".as_bytes()).unwrap();
    println!("Starting loop! Do Ctrl-C at any time to exit.");
    // Below the code can be seperated into two sections, the server and the client.
    // If you were doing this on different programs you'd just copy the code for each section into the separate programs.
    loop {
        // Server update. If there are any errors, they will happen here.
        server.update().unwrap();
        // Receive packets from the server
        while let Some(event) = server.poll() {
            match event {
                ServerEvent::ReceivedPacket(received) => {
                    println!("Server received packet from client {} with data {}", received.get_id(), String::from_utf8_lossy(&received.get_packet()));
                    // Echo the packet back to the client
                    server.send(received.get_id(), &received.get_packet()).unwrap();
                }
                ServerEvent::ClientConnectionStatus(status) => {
                    println!("Client {} {}.", status.get_id(), match status.get_status() {
                        ConnectionStatus::Connected => { "connected" }
                        ConnectionStatus::Disconnected => { "disconnected" }
                    });
                }
            }
        }
        // --------------------------------
        // Poll for packets from the server
        loop {
            match client.update() {
                Ok(res) => {
                    if let Some(packet) = res {
                        println!("Client received packet with data {}", String::from_utf8_lossy(&packet));
                        // Echo the packet back to the server.
                        client.send(&packet).unwrap();
                    } else {
                        break;
                    }
                }
                Err(err) => {
                    panic!("{}", err);
                }
            }
        }
        // Sleep to slow down how many packets are sent.
        sleep(Duration::from_millis(500));
    }
}