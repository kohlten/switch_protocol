# Switch Protocol

----

Switch Protocol is a simple lightweight protocol around a stream based socket that allows for packet based parsing
instead of stream based. This is useful for when you need the aspects of say Tcp, but are not streaming data. Allows for
easier packet parsing on the ends. This library also abstracts over the details of stream based protocols, so you can
just poll for events on the stream instead. See the echo example for details on how to use.

The protocol itself is called the "Switch Protocol", because the internal parser uses a state machine as a means to
parse the packets. The data will come in with a header. The parser will then parse that data, and wait for the body of
the packet to arrive in full. A very simple and lightweight protocol that allows for significantly easier parsing.